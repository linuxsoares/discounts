DEV_SETTINGS=discounts.settings.development
PRODUCTION_SETTINGS=discounts.settings.production
TEST_SETTINGS=discounts.settings.testÏ

run:
	@DJANGO_SETTINGS_MODULE=$(DEV_SETTINGS) gunicorn -b 0.0.0.0:8000 -w 1 --pythonpath django discounts.wsgi

migrate:
	./manage.py migrate --settings=$(DEV_SETTINGS)

makemigrations:
	./manage.py makemigrations --settings=$(DEV_SETTINGS)

run-celery:
	DJANGO_SETTINGS_MODULE=$(DEV_SETTINGS) celery -A discounts worker -B -l DEBUG --autoscale=10,3

shell:
	./manage.py shell --settings=$(DEV_SETTINGS)