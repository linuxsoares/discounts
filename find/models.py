from django.db import models

OPTIONS_NOTIFY = (
    ('E-MAIL', 'EMAIL'),
    ('TELEGRAM', 'TELEGRAM'),
    ('SMS', 'SMS')
)


class Discounts(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    value = models.FloatField(blank=False, null=False, max_length=10)
    product = models.CharField(blank=False, max_length=200)


class NotificateUser(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    find_by = models.CharField(blank=False, max_length=200)
    notify_by = models.CharField(choices=OPTIONS_NOTIFY, default='EMAIL', max_length=100)
    contact = models.CharField(blank=False, max_length=200)
