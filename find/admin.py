from django.contrib import admin

from find.models import Discounts, NotificateUser


@admin.register(Discounts)
class DiscountsAdmin(admin.ModelAdmin):
    list_display = (
        'value',
        'product'
    )


@admin.register(NotificateUser)
class NotifyAdmin(admin.ModelAdmin):
    list_display = (
        'notify_by',
        'date_created',
        'find_by'
    )
