# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-05-20 17:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('find', '0004_notificateuser_find_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificateuser',
            name='contact',
            field=models.CharField(default=django.utils.timezone.now, max_length=200),
            preserve_default=False,
        ),
    ]
