import requests

from celery import shared_task
from bs4 import BeautifulSoup
from commom.email import Email
import logging

from find.models import Discounts, NotificateUser

logger = logging.getLogger(__name__)


@shared_task
def find_discounts(items=None):
    if not items:
        return
    url = 'https://www.nescafe-dolcegusto.com.br/combos-ndg'
    # url = 'https://www.google.com.br'
    soup = BeautifulSoup(
        _get_html(url)
    )
    mydivs = soup.find_all("div", {"class": "single-kit"})
    for item in items:
        for div in mydivs:
            verify = _validate_item(div, item)

        logger.info(
            'Verificar {} {}'.format(
                item,
                'SIM' if verify else 'NÃO'
            )
        )
    return True


def _validate_item(div, item):
    verify = False
    h5 = div.find_all('h5', {'class': 'title'})
    if h5[0].text.upper() == item:
        spans = div.find_all('span', {'class': 'product_widget_price'})
        for span in spans:
            value = float(
                span.text.strip().replace('R$', '').replace(',', '.')
            )
            if not value:
                logger.error('Valor não encontrado.')
            if value < Discounts.objects.get(
                product='cafe'
            ).value:
                notify_by = NotificateUser.objects.values_list(
                    'notify_by',
                    flat=True
                ).filter(
                    find_by='cafe'
                )[::1]
                _notificate_user.delay(item=item, value=value, notify_by=notify_by)
                verify = True
    return verify


def _get_html(url):
    response = requests.get(url)
    data = response.text
    return data


@shared_task
def _notificate_user(item, value, notify_by=None):
    if not notify_by:
        return
    if 'E-MAIL' in notify_by:
        context = {'item': item, 'value': value}
        to = NotificateUser.objects.values_list(
            'contact',
            flat=True
        ).filter(
            find_by='cafe'
        )[::1]
        email = Email(
            to=to,
            subject='ALERT - DOLCE GUSTO',
            template='alert-dolce-gusto.html',
            create_context=context
        )
        email.send()
    if 'TELEGRAM' in notify_by:
        pass
    if 'SMS' in notify_by:
        pass
