import pytz
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.html import strip_tags


class EmailContextException(Exception):
    pass


class Email:

    def __init__(
            self,
            create_context=None,
            to=None,
            subject=None,
            template=None
    ):
        if not to:
            raise EmailContextException(
                'Parameter to can not be empty.'
            )
        if not subject:
            raise EmailContextException(
                'Parameter subject can not be empty.'
            )
        if not template:
            raise EmailContextException(
                'Parameter template can not be empty.'
            )
        self._create_context = create_context
        self._format = "%d/%m/%Y %H:%M:%S"
        self._to = to
        self.subject = subject
        self._html = ''
        self._text = ''
        self.render_html(template=template)
        self.files = []

    @property
    def html(self):
        return self._html

    def attach_files(self, files=[]):
        for file in files:
            self.files.append(file)

    def render_html(self, template):
        self._html = render_to_string(template, self.context())

    def context(self):
        date_send_email = timezone.now().astimezone(
            pytz.timezone('America/Sao_Paulo')
        ).strftime(self._format)
        dictonary = {
            'date_send_email': date_send_email,
            'context': self._create_context
        }
        return dictonary

    def send(self, from_addr=None, fail_silently=False):
        if isinstance(self._to, str):
            self._to = [self._to]
        if not from_addr:
            from_addr = settings.EMAIL_FROM_ADDR
        msg = EmailMultiAlternatives(
            self.subject,
            self._text,
            from_addr,
            self._to,
        )
        if self._html:
            msg.body = strip_tags(self._html)

        if self.files:
            for file in self.files:
                msg.attach_file(file)
        return msg.send(fail_silently)
