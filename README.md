# DISCOUNTS
Aplicação utilizada para encontrar descontos no SITE Dolce Gusto.

# Requisitos
- Python 3
- RabbitMQ

# Desenvolvimento
Todos os comandos rodar testes, rodar a aplicação `Makefile` na raiz do projeto.

## Instalação
- Crie e ative uma [virtualenv](https://virtualenv.pypa.io/) com Python 3.

## Testes
- Necessario adicionar.

## Executando
```
O arquivo a ser analisado deve estar dentro do diretório `logs`.
```
- Rodando o projeto:
    `make run`
    `make run-celery`